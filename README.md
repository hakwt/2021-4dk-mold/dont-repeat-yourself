# Don't repeat yourself

Die GUI-Anwendung in diesem Repository zählt die Klicks auf einen Button und gibt die Anzahl aus.

Die zentrale Funktionalität dieser Anwendung - das Zählen der Klicks - meist Code-Duplizierung auf.
Finde eine Lösung, damit das DRY Prinzip nicht verletzt wird.

### (für später) Zusatzaufgabe: Open-Closed-Principle

Es soll ein zusätzlicher `Button C` hinzugefügt werden, dessen Klicks genauso gezählt werden. Wenn du das machen kannst,
ohne die Zähllogik und die Anzeige der Zähler ändern zu müssen, wurde das OCP eingehalten. Falls nicht, finde eine
Lösung die das OCP berücksichtigt.