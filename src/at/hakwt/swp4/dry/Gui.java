package at.hakwt.swp4.dry;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Gui extends JFrame {

    private int countA = 0;
    private int countB = 0;

    public Gui() {

        super("ButtonClickCounting");

        JPanel panel = new JPanel();
        JButton button = new JButton("How many clicks?");
        JLabel label = new JLabel("No of clicks");
        JButton buttonA = new JButton("Button A");
        JButton buttonB = new JButton("Button B");

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label.setText("A: " + countA + ", B: " + countB);
            }
        });
        panel.add(button);
        panel.add(label);

        buttonA.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                countA++;
            }
        });

        buttonB.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                countB++;
            }
        });

        panel.add(buttonA);
        panel.add(buttonB);
        add(panel);
        setVisible(true);
        setSize(500, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}
